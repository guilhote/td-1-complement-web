<?php

////////////////////
// Initialisation //
////////////////////

require_once __DIR__ . '/../vendor/autoload.php';


/////////////
// Routage //
/////////////

TheFeed\Controleur\RouteurURL::traiterRequete();

