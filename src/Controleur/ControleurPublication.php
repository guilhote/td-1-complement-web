<?php

namespace TheFeed\Controleur;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Service\Exception\ServiceException;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Service\PublicationServiceInterface;

class ControleurPublication extends ControleurGenerique
{
    public function __construct(private readonly PublicationServiceInterface $publicationService)
     {}

    #[Route(path: '/', name: 'afficherListeConnecte', methods: ["GET"])]

    #[Route(path: '/publications', name: 'afficherListe', methods: ["GET"])]
    /**
     * @throws Exception
     */
    public function afficherListe(): Response
    {
        $publications = $this->publicationService->recupererPublications();
        return self::afficherTwig('publication/feed.html.twig', [
            "publications" => $publications,
            "pagetitle" => "The Feed"
        ]);
    }

    #[Route(path: '/publications', name: 'creerPublication', methods: ["POST"])]
    public function creerDepuisFormulaire(): Response
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try {
            $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return self::rediriger('afficherListe');
        }
        return self::rediriger('afficherListe');
    }


}