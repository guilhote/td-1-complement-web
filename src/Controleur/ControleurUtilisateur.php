<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\MessageFlash;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurServiceInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ControleurUtilisateur extends ControleurGenerique
{
    public function __construct(private readonly UtilisateurServiceInterface $utilisateurService, private readonly PublicationServiceInterface $publicationService)
     {}

    public function afficherErreur($messageErreur = "", $statusCode = ""): \Symfony\Component\HttpFoundation\Response
    {
        return parent::afficherErreur($messageErreur, "utilisateur");
    }

    /**
     * @throws \Exception
     */
    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name: 'creerDepuisFormulairePublications', methods: ["GET"])]
    public function afficherPublications($idUtilisateur): Response
    {
        try {
            $utilisateur = $this->utilisateurService->recupererUtilisateurParId($idUtilisateur, false);
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
        }
        $publications = $this->publicationService->recupererPublicationsUtilisateur($idUtilisateur);
        $loginHTML = htmlspecialchars($utilisateur->getLogin());
        return self::afficherTwig('publication/page_perso.html.twig', [
            "publications" => $publications,
            "pagetitle" => $loginHTML
        ]);
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    #[Route(path: '/inscription', name: 'afficherFormulaireCreation', methods: ["GET"])]
    public function afficherFormulaireCreation(): Response
    {
        return $response = self::afficherTwig("utilisateur/inscription.html.twig", [
            "pagetitle" => "Création d'un utilisateur",
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
    }

    #[Route(path: '/inscription', name: 'creerDepuisFormulaire', methods: ["POST"])]
    public function creerDepuisFormulaire(): Response
    {
        //Recupérer les différentes variables (login, mot de passe, adresse mail, données photo de profil...)
        $login = $_POST['login'];
        $motDePasse = $_POST['mot-de-passe'];
        $adresseMail = $_POST['email'];
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;
        echo $login;
        echo $motDePasse;
        echo $adresseMail;
        try {
            $this->utilisateurService->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
        } catch (ServiceException $e) {
            //Ajouter message flash d'erreur
            MessageFlash::ajouter("error", $e->getMessage());
            //Rediriger sur le formulaire de création
            return self::rediriger("afficherFormulaireCreation");
        }
        //Ajouter un message flash de succès (L'utilisateur a bien été créé !)
        MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
        //Rediriger sur la page d'accueil (route afficherListe)
        return self::rediriger('afficherListe');
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    #[Route(path: '/connexion', name: 'afficherFormulaireConnexion', methods: ["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
        return self::afficherTwig("utilisateur/connexion.html.twig", [
            "pagetitle" => "Formulaire de connexion",
            "cheminVueBody" => "utilisateur/formulaireConnexion.php",
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
    }

    #[Route(path: '/connexion', name: 'connecter', methods: ["POST"])]
    public function connecter(): Response
    {
        try {
            $this->utilisateurService->connecterUtilisateur($_POST['login'], $_POST['mot-de-passe']);
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return self::rediriger("afficherFormulaireConnexion");
        }
        MessageFlash::ajouter("success", "Connexion effectuée.");
        return self::rediriger("afficherListe");
    }

    #[Route(path: '/deconnexion', name: 'deconnecter', methods: ["GET"])]
    public function deconnecter(): Response
    {
        try {
            $this->utilisateurService->deconnecterUtilisateur();
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return self::rediriger("afficherListe");
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return self::rediriger("afficherListe");
    }
}
