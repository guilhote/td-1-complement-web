<?php

namespace TheFeed\Service;

use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface
{
    public function __construct(private readonly UtilisateurRepository $utilisateurRepository)
     {}

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur
    {
        $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur) ?? null;
     if (!$autoriserNull && $utilisateur == null) {
        throw new ServiceException("L'utilisateur n'existe pas");
    }
     return $utilisateur;
 }

    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil): void
    {
        if (!isset($login) || !isset($motDePasse) || !isset($email) || !isset($donneesPhotoDeProfil)) {
            throw new ServiceException("Login, nom, prenom ou mot de passe manquant.");
        }
        if (strlen($login) < 4 || strlen($login) > 20) {
            throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");
        }
        if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
            throw new ServiceException("Mot de passe invalide!");
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ServiceException("L'adresse mail est incorrecte!");
        }

        $utilisateurRepository = $this->utilisateurRepository;
        $utilisateur = $utilisateurRepository->recupererParLogin($login);
        if ($utilisateur != null) {
            throw new ServiceException("Ce login est déjà pris!");
        }

        $utilisateur = $utilisateurRepository->recupererParEmail($email);
        if ($utilisateur != null) {
            throw new ServiceException("Un compte est déjà enregistré avec cette adresse mail!");
        }

        $mdpHache = MotDePasse::hacher($motDePasse);

        // Upload des photos de profil
        // Plus d'informations :
        // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

        // On récupère l'extension du fichier
        $explosion = explode('.', $donneesPhotoDeProfil['name']);
        $fileExtension = end($explosion);
        if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
            throw new ServiceException("La photo de profil n'est pas au bon format!");
        }
        // La photo de profil sera enregistrée avec un nom de fichier aléatoire
        $pictureName = uniqid() . '.' . $fileExtension;
        $from = $donneesPhotoDeProfil['tmp_name'];
        $to = __DIR__ . "/../../ressources/img/utilisateurs/$pictureName";
        move_uploaded_file($from, $to);

        $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
        $utilisateurRepository->ajouter($utilisateur);

    }

    /**
     * @throws ServiceException
     */
    public function connecterUtilisateur($login, $motDePasse): void
    {
        if (!(isset($login) && isset($motDePasse))) {
            throw new ServiceException("Login ou mot de passe manquant.");
        }
        $utilisateurRepository = $this->utilisateurRepository;
        /** @var Utilisateur $utilisateur */
        $utilisateur = $utilisateurRepository->recupererParLogin($login);

        if ($utilisateur == null) {
            throw new ServiceException("Login inconnu.");
        }

        if (!MotDePasse::verifier($motDePasse, $utilisateur->getMdpHache())) {
            throw new ServiceException("Mot de passe incorrect.");
        }

        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
    }

    /**
     * @throws ServiceException
     */
    public function deconnecterUtilisateur(): void
    {
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException("Utilisateur non connecté.");
        }
        ConnexionUtilisateur::deconnecter();
    }

}