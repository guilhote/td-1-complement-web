<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;

class PublicationServiceTest extends TestCase
{
    private PublicationService $service;

    protected function setUp(): void {
        parent::setUp();
        $this->service = new PublicationService();
    }

    /**
     * @throws ServiceException
     */
    public function testCreerPublicationUtilisateurInexistant() {
        $this->expectException(ServiceException::class);
        $this->service->creerPublication(-1, "Test");
    }

}